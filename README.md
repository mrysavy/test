# Test

## Technologie
Tento LAB je zaměřen na různá nastavení `Ingress` z pohledu TLS certifikátů.

Použijeme tyto technologie:
- `AKS` - prostředí
- `NGINX` - ingress controller
- `CertManager` - správce certifikátů
- `Let's Encrypt` - vydavatel certifikátů

Pro usnadnění práce použijeme tyto doplňkové technologie:
- `Azure DNS` - správa DNS záznamů
- `External DNS` - automatický provisioning DNS záznamů
- `Hello-kubernetes` - testovací aplikace

## Proměnné
Pro správnou funkci zde uvedených snippetů je potřeba správně nastavit tyto proměnné prostředí:

```bash
# Resource group name pro AKS a DNS zonu (az group list -o table)
export AKS_RG=<AKS_RG>
# Resource group s AKS VM (az group list -o table)
export AKS_RG_VM=<AKS_RG_VM>
# Jmeno AKS clusteru (az aks list -o table)
export AKS_NAME=<AKS_NAME>
# Jmeno DNS zony (az network dns zone list -o table)
export AKS_DNS=<AKS_DNS>
# Email pro Let's encrypt
export LE_EMAIL=<LE_EMAIL>
```

## Prerekvizity
Začneme prerekvizitami:

<details>
<summary>Azure Kubernetes Services (AKS)</summary>

Postup pro založení nového <code>AKS</code> clusteru (pozor na náklady) zde. Případně lze použít stávající cluster, avšak některé operace mohou být destruktivní.

```bash
# TBD
```
</details>

<details>
<summary>Azure DNS zóna</summary>

Postup pro založení nové DNS zóny zde. Případně lze použít stávající zónu, postup však počítá, že je ve stejné <code>ResourceGroup</code> jako <code>AKS</code> cluster

```bash
# TBD
```
</details>

<details>
<summary><code>kubeconfig</code> AKS clusteru</summary>

Nakonec postup pro stažení <code>kubeconfig</code> a nastavení <code>KUBECONFIG</code>

```bash
# Stazeni kubeconfigu
az aks get-credentials --admin --name "${AKS_NAME}" --resource-group "${AKS_RG}" -f - > kubeconfig
# Nastaveni odpovidajiciho KUBECONFIG
export KUBECONFIG=`pwd`/kubeconfig
```
</details>

## Příprava
Prerekvizity jsou připraveny a lze instalovat jednotlivé součásti, které se v labu využijí

* [AAD Pod Identity](prepare/aad-pod-identity.md)
* [External DNS](prepare/external-dns.md)
* [Cert manager](prepare/cert-manager.md)
* [Wildcard certifikát](prepare/wildcard-certifikat.md)
* [NGINX Ingress Controller](prepare/nginx-ingress-controller.md)
* [Hello Kubernetes](prepare/hello-kubernetes.md)
* [TLS proxy k hello-world aplikaci](prepare/tls-proxy.md)

## Úkoly

### Zpřístupnit aplikaci nešifrovaně jako `http://hello-insecure.<DOMAIN>/`
1. **Vystavit** připravenou aplikaci `hello-kubernetes` nešifrovaně pod doménovým jménem `hello-insecure.<DOMAIN>` s maximálním využitím připravených komponent.
1. **Ověřit** že aplikace funguje na daném URL
1. **Prozkoumat** jaký je rozdíl mezi `pathType: Exact` a `pathType: Prefix`
1. **Prozkoumat** jak vznik DNS záznam pro dané URL
