# Wildcard cartifikát

Wildcard (hvězdičkový) certifikát využijeme jako výchozí certifikát pro `Ingress Controller`. Pro jeho vystavení využijeme `Cert manager`. Jako `commonName` i `dnsName` použijeme wildcard doménu `*.<DNS ZONE>`. A chceme certifikát od `Let's Encrypt` aby byl důvěryhodný v prohlížeči.

Deployment sestává z následujících kroků:

### iniciace žádosti o certifikát

```yaml
cat <<EOF | kubectl create -f-
apiVersion: cert-manager.io/v1
kind: Certificate
metadata:
  name: wildcard
  namespace: cert-manager
spec:
  secretName: wildcard-cert
  duration: 2160h # 90d
  renewBefore: 360h # 15d
  isCA: false
  privateKey:
    algorithm: RSA
    encoding: PKCS1
    size: 2048
  # Wildcard domena
  commonName: "*.${AKS_DNS}"
  usages:
    - server auth
    - client auth
  # Wildcard domena
  dnsNames:
    - "*.${AKS_DNS}"
  # Let' Encrypt issuer
  issuerRef:
    name: letsencrypt-prod
    kind: ClusterIssuer
    group: cert-manager.io
EOF
```
Celý LAB počítá s umístěním v namespace `cert-manager`.
