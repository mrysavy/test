# External DNS

[External DNS](https://github.com/kubernetes-sigs/external-dns) je služba, která na základě `Ingress` / `Service` resourců automaticky vytváří DNS záznamy. Pro přístup k `Azure` (`Azure DNS`) API využije `AAD Pod Identity`.

Deployment sestává z následujících kroků:

### deployment helm chartu

Pozor verze 0.10 nefunguje (viz. [zde](https://github.com/kubernetes-sigs/external-dns/pull/2384))

```bash
# Nastavení Helm repository na klientovi
helm repo add bitnami https://charts.bitnami.com/bitnami
helm repo update

# Deployment Helm chartu
helm \
  # Install nebo upgrade & pripadne vytvoreni namespace \
  upgrade --install --create-namespace \
  # ReleaseName & HelmChart & Namespace \
  external-dns bitnami/external-dns -n external-dns \
    # Verze Helm chartu \
    --version 5.4.8 \
    # Zakladani i mazani \
    --set policy=sync \
    # Azure DNS \
    --set provider=azure \
    --set azure.tenantId=$(az aks show -n "${AKS_NAME}" -g "${AKS_RG}" --query identity.tenantId -otsv) \
    --set azure.subscriptionId=$(az account show --query id -otsv) \
    --set azure.resourceGroup=${AKS_RG} \
    --set azure.useManagedIdentityExtension=true \
    # Pouziti AAD Pod Identity \
    --set podLabels.aadpodidbinding=kubeletidentity
```

Celý LAB počítá s umístěním v namespace `external-dns`.
