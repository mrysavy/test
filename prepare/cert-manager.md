# Cert manager

[Cert manager](https://github.com/jetstack/cert-manager) je služba, která automaticky připravuje certifikáty pro `Ingress` resource.
Cert manager umí `ClusterIssuer` i `Issuer`. `Issuer` má platnost pouze v daném namespace, kde je vytvořen, kdežto `ClusterIssuer` má platnost v celém clusteru.

Deployment sestává z následujících kroků:

### deployment helm chartu

```bash
# Nastavení Helm repository na klientovi
helm repo add jetstack https://charts.jetstack.io
helm repo update

# Deployment Helm chartu
helm \
  # Install nebo upgrade & pripadne vytvoreni namespace \
  upgrade --install --create-namespace \
  # ReleaseName & HelmChart & Namespace \
  cert-manager jetstack/cert-manager -n cert-manager \
  # Vcetne CRD \
  --set installCRDs=true
```
Celý LAB počítá s umístěním v namespace `cert-manager`.

### příprava Let's Encrypt `ClusterIssuer` (http i dns)

```yaml
export AKS_SUBSCRIPTION=$(az account show --query id -otsv)

cat <<EOF | kubectl create -f -
apiVersion: cert-manager.io/v1
kind: ClusterIssuer
metadata:
  name: letsencrypt-prod
spec:
  acme:
    server: https://acme-v02.api.letsencrypt.org/directory
    email: ${LE_EMAIL}
    privateKeySecretRef:
      name: letsencrypt-prod
    solvers:
    - http01:
        ingress:
          class: nginx
    - dns01:
        azureDNS:
          subscriptionID: ${AKS_SUBSCRIPTION}
          resourceGroupName: ${AKS_RG}
          hostedZoneName: ${AKS_DNS}
          environment: AzurePublicCloud
EOF
```

### příprava Self-signed `ClusterIssuer`

```bash
cat <<EOF | kubectl create -f -
apiVersion: cert-manager.io/v1
kind: ClusterIssuer
metadata:
  name: selfsigned
spec:
  selfSigned: {}
EOF
```
