# Hello Kubernetes

[Hello-kubernetes](https://github.com/paulbouwer/hello-kubernetes) je aplikace slouží jako testovací aplikace, na které budou vidět rozdíly v jednotlivých nastaveních `Ingress` objektů.

Deployment sestává z následujících kroků:

### deployment helm chartu

```bash
git clone https://github.com/paulbouwer/hello-kubernetes.git
helm \
  # Install nebo upgrade & pripadne vytvoreni namespace \
  upgrade --install --create-namespace \
  # ReleaseName & HelmChart & Namespace \
  hello-kubernetes hello-kubernetes/deploy/helm/hello-kubernetes/ -n hello-kubernetes
```

Celý LAB počítá s umístěním v namespace `hello-kubernetes`.
