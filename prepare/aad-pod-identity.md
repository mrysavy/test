# AAD Pod Identity

[AAD Pod Identity](https://github.com/Azure/aad-pod-identity) je služba, která zpřístupní `AAD identity` do `Pod`. Využijeme ji pro `External DNS` a `Let's Encrypt` DNS Issuer

Deployment sestává z následujících kroků:

### deployment helm chartu

```bash
# Nastavení Helm repository na klientovi
helm repo add aad-pod-identity https://raw.githubusercontent.com/Azure/aad-pod-identity/master/charts
helm repo update

# Deployment Helm chartu
helm \
  # Install nebo upgrade & pripadne vytvoreni namespace \
  upgrade --install --create-namespace \
  # ReleaseName & HelmChart & Namespace \
  aad-pod-identity aad-pod-identity/aad-pod-identity -n aad-pod-identity \
  # postaci jedna replika \
  --set mic.replicas=1
```

### vytvoření potřebných rolí v `Azure`

```bash
export AKS_KUBELET_CLIENTID=$(az aks show -n "${AKS_NAME}" -g "${AKS_RG}" --query identityProfile.kubeletidentity.clientId -o tsv)
export AKS_DNS_ZONE_ID=$(az network dns zone show -n "${AKS_DNS}" -g "${AKS_RG}" --query id -o tsv)

az role assignment create --role "DNS Zone Contributor" \
  --assignee ${AKS_KUBELET_CLIENTID} \
  --scope ${AKS_DNS_ZONE_ID}
az role assignment create --role "Reader" \
  --assignee ${AKS_KUBELET_CLIENTID} \
  -g "${AKS_RG}"
az role assignment create --role "Reader" \
  --assignee ${AKS_KUBELET_CLIENTID} \
  -g "${AKS_RG_VM}"
```

### založení `AzureIdentity` a `AzureIdentityBinding`

```yaml
export AKS_KUBELET_RESOURCEID=$(az aks show -n "${AKS_NAME}" -g "${AKS_RG}" --query identityProfile.kubeletidentity.resourceId)
export AKS_KUBELET_CLIENTID=$(az aks show -n "${AKS_NAME}" -g "${AKS_RG}" --query identityProfile.kubeletidentity.clientId)

cat <<EOF | kubectl create -f -
apiVersion: "aadpodidentity.k8s.io/v1"
kind: AzureIdentity
metadata:
  name: kubeletidentity
  namespace: aad-pod-identity
spec:
  type: 0
  resourceID: ${AKS_KUBELET_RESOURCEID}
  clientID: ${AKS_KUBELET_CLIENTID}
---
apiVersion: "aadpodidentity.k8s.io/v1"
kind: AzureIdentityBinding
metadata:
  name: kubeletidentity-binding
  namespace: aad-pod-identity
spec:
  azureIdentity: kubeletidentity
  selector: kubeletidentity
EOF
```
