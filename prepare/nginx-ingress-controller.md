# External DNS

[External DNS](https://github.com/kubernetes-sigs/external-dns) je služba, která na základě `Ingress` / `Service` resourců automaticky vytváří DNS záznamy. Pro přístup k `Azure` (`Azure DNS`) API využije `AAD Pod Identity`.

Deployment sestává z následujících kroků:

### deployment helm chartu

```bash
# Nastavení Helm repository na klientovi
helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx
helm repo update

# Deployment Helm chartu
helm \
  # Install nebo upgrade & pripadne vytvoreni namespace \
  upgrade --install --create-namespace \
  # ReleaseName & HelmChart & Namespace \
  nginx ingress-nginx/ingress-nginx -n ingress-nginx \
    # Nastaveni ingress class \
    --set controller.ingressClass=nginx \
    # Povoleni SSL passthrough \
    --set controller.extraArgs.enable-ssl-passthrough= \
    # Nastaveni vychoziho certifikatu \
    --set controller.extraArgs.default-ssl-certificate=cert-manager/wildcard-cert
```

Celý LAB počítá s umístěním v namespace `ingress-nginx`.
