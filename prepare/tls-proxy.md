# TLS proxy k hello-world aplikaci

Protože Hello-kubernetes aplikace nepodporuje šifrované připojení (podporuje pouze HTTP, HTTPS není podporováno), pro simulaci HTTPS deploymentu použijeme NGINX jako TLS proxy. Jako `commonName` i `dnsName` použijeme `tls-proxy` řetězec.

Deployment sestává z následujících kroků:

### příprava self-signed certifikátu

```yaml
cat <<EOF | kubectl create -f -
apiVersion: cert-manager.io/v1
kind: Certificate
metadata:
  name: tls-proxy
  namespace: hello-kubernetes
spec:
  secretName: tls-proxy-cert
  duration: 2160h # 90d
  renewBefore: 360h # 15d
  isCA: false
  privateKey:
    algorithm: RSA
    encoding: PKCS1
    size: 2048
  commonName: tls-proxy
  usages:
    - server auth
    - client auth
  # At least one of a DNS Name, URI, or IP address is required.
  dnsNames:
    - tls-proxy
  # Issuer references are always required.
  issuerRef:
    name: selfsigned
    kind: ClusterIssuer
    group: cert-manager.io
EOF
```

Celý LAB počítá s umístěním v namespace `hello-kubernetes`.

### vytvoření Deployment + Service + ConfigMap

```yaml
cat <<EOF | kubectl create -f -
apiVersion: apps/v1
kind: Deployment
metadata:
  name: tls-proxy
  namespace: hello-kubernetes
spec:
  selector:
    matchLabels:
      app.kubernetes.io/name: tls-proxy
  template:
    metadata:
      labels:
        app.kubernetes.io/name: tls-proxy
    spec:
      containers:
        - name: nginx-proxy
          image: nginx:1.19.5
          volumeMounts:
            - name: nginx-config
              mountPath: /etc/nginx/conf.d
              readOnly: true
            - name: certs
              mountPath: /certs
              readOnly: true
          ports:
            - name: https
              containerPort: 8443
      volumes:
        - name: nginx-config
          configMap:
            name: tls-proxy-nginx
        - name: certs
          secret:
            secretName: tls-proxy-cert
---
apiVersion: v1
kind: Service
metadata:
  name: tls-proxy
  namespace: hello-kubernetes
spec:
  ports:
  - name: https
    port: 443
    protocol: TCP
    targetPort: https
  selector:
    app.kubernetes.io/name: tls-proxy
  type: ClusterIP
---
apiVersion: v1
kind: ConfigMap
metadata:
  name: tls-proxy-nginx
  namespace: hello-kubernetes
data:
  tls-proxy.conf: |
    server {
        listen 8443 ssl;
        server_name test;
        ssl_certificate /certs/tls.crt;
        ssl_certificate_key /certs/tls.key;

        location / {
            proxy_pass http://hello-kubernetes-hello-kubernetes.hello-kubernetes.svc.cluster.local:80;
        }
    }
EOF
```

Celý LAB počítá s umístěním v namespace `hello-kubernetes`.
