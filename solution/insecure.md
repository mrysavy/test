# Zpřístupnit aplikaci nešifrovaně jako `http://hello-insecure.<DOMAIN>/`

### 1. **Vystavit** připravenou aplikaci `hello-kubernetes` nešifrovaně pod doménovým jménem `hello-insecure.<DOMAIN>` s maximálním využitím připravených komponent.

V cluster již máme běžící aplikaci `Hello Kubernetes` a funkční `Ingress Controller`.

Pro dokončení úkolu je tedy nezbytné:
<details>
<summary>vytvořit `Ingress` resource</summary>

`Ingress` resource bude bez `spec.tls`, neboť se má jednat o nešifrované spojení.

```yaml
cat <<EOF | kubectl create -f -
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: insecure
  namespace: hello-kubernetes
spec:
  ingressClassName: nginx
  rules:
  - host: hello-insecure.${AKS_DNS}
    http:
      paths:
      - backend:
          service:
            name: hello-kubernetes-hello-kubernetes
            port:
              number: 80
        path: /
        pathType: Prefix
EOF
```
</details>

### 2. **Ověřit** že aplikace funguje na daném URL

Použít browser a otevřít dané URL (potřeba upravit <DOMAIN> za ${AKS_DNS}).

<details>
<summary>Očekávaný výstup vypadá zhruba takto</summary>

![](solution/insecure-output.png)
</details>

### 3. **Prozkoumat** jaký je rozdíl mezi `pathType: Exact` a `pathType: Prefix`

